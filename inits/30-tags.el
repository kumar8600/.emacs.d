;;; 30-tags.el --- My emacs configuration for tags

;; Author: kumar8600 <kumar8600@gmail.com>

;; Copyright (c) 2014 by kumar8600
;; All rights reserved.

;;; Commentary:

;;; Code:


;; require https://raw.github.com/voins/gtags/master/gtags.el
(require 'gtags)

;; ;; anything-gtags
;; (setq gtags-prefix-key "\C-c")
;; (require 'anything-gtags)
;; ;; キーバインド
;; (setq gtags-mode-hook
;;       '(lambda ()
;; 	 (define-key gtags-mode-map "\C-cs" 'gtags-find-symbol)
;; 	 (define-key gtags-mode-map "\C-cr" 'gtags-find-rtag)
;; 	 (define-key gtags-mode-map "\C-ct" 'gtags-find-tag)
;; 	 (define-key gtags-mode-map "\C-cf" 'gtags-parse-file)))
;; ;; gtags-mode を使いたい mode の hook に追加する
;; (add-hook 'c-mode-common-hook
;; 	  '(lambda()
;; 	     (gtags-mode 1)))

;; ;; update GTAGS
;; (defun update-gtags (&optional prefix)
;;   (interactive "P")
;;   (let ((rootdir (gtags-get-rootpath))
;; 	(args (if prefix "-v" "-iv")))
;;     (when rootdir
;;       (let* ((default-directory rootdir)
;; 	     (buffer (get-buffer-create "*update GTAGS*")))
;; 	(save-excursion
;; 	  (set-buffer buffer)
;; 	  (erase-buffer)
;; 	  (let ((result (process-file "gtags" nil buffer nil args)))
;; 	    (if (= 0 result)
;; 		(message "GTAGS successfully updated.")
;; 	      (message "update GTAGS error with exit status %d" result))))))))
;; (add-hook 'after-save-hook 'update-gtags)

;;; 30-tags.el ends here
