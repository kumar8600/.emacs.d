;;; 20-appearance.el --- My emacs configurations for appearance

;; Author: kumar8600 <kumar8600@gmail.com>

;; Copyright (c) 2014 by kumar8600
;; All rights reserved.

;;; Commentary:

;;; Code:

;; Flycheck Color Mode Line
(eval-after-load "flycheck"
  '(add-hook 'flycheck-mode-hook 'flycheck-color-mode-line-mode))

;; faceを設定
(custom-set-faces
 '(flycheck-color-mode-line-error-face ((t
					 :foreground "#efefef"
					 :background "#ed7676"
					 :weight normal
					 :inherit flycheck-fringe-error)))
 '(flycheck-color-mode-line-warning-face ((t
					   :foreground "#efefef"
					   :background "#eaea75"
					   :weight normal
					   :inherit flycheck-fringe-warning)))
 '(flycheck-color-mode-line-info-face ((t
					:foreground "#efefef"
					:background "#75a4ea"
					:weight normal
					:inherit flycheck-fringe-warning))))

;; テーマ

(add-to-list 'load-path "~/.emacs.d/site-lisp/all-system/themes")
(require 'vim-colors-theme)
(load-theme 'vim-colors t) ;; デフォルトの背景色を使うように改造済み

;; モードライン
(require 'smart-mode-line)

(sml/setup)

;;; 20-appearance.el ends here
