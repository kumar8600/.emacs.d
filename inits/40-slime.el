;;; 40-slime.el --- My emacs configuration for Slime.

;; Author: kumar8600 <kumar8600@gmail.com>

;; Copyright (c) 2014 by kumar8600
;; All rights reserved.

;;; Commentary:

;;; Code:

(require 'slime)

;; CLISPをデフォルトのCommon Lisp処理系に設定
(setq inferior-lisp-program "clisp")

;; (require 'slime-company)
;; ;; company
;; (slime-setup '(slime-company))


;;; 40-slime.el ends here
