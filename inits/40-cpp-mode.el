;;; 40-cpp-mode.el --- My emacs configuration for c++-mode.

;; Author: kumar8600 <kumar8600@gmail.com>

;; Copyright (c) 2014 by kumar8600
;; All rights reserved.

;;; Commentary:

;;; Code:

;; .h open as C++ header
(add-to-list 'auto-mode-alist '("\\.h\\'" . c++-mode))

(require 'auto-complete)
(require 'yasnippet)
;; (require 'irony) ;Note: hit `C-c C-b' to open build menu

;; the ac plugin will be activated in each buffer using irony-mode
;; (irony-enable 'ac)             ; hit C-RET to trigger completion

(defun my-c++-hooks ()
  "Enable the hooks in the preferred order: 'yas -> auto-complete -> irony'."
  ;; be cautious, if yas is not enabled before (auto-complete-mode 1), overlays
  ;; *may* persist after an expansion.
  (yas-minor-mode-on)
  (auto-complete-mode 1)

  ;; avoid enabling irony-mode in modes that inherits c-mode, e.g: php-mode
  ;; (when (member major-mode irony-known-modes)
  ;;   (irony-mode 1)))
  )

(add-hook 'c++-mode-hook 'my-c++-hooks)
(add-hook 'c-mode-hook 'my-c++-hooks)

;;; 40-cpp-mode.el ends here
