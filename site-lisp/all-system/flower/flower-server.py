# -*- coding: utf-8 -*-

"""Flower server

This module provides server for Flower mode that emacs minor mode of libclang
integration.

"""

__author__ = "kumar8600 <kumar8600@gmail.com>"
__copyright__ = "Copyright 2014, kumar8600"
__license__ = "BSD 3-Clause"
#__version__ = "$Revision$"

from epc.server import EPCServer
import clang.cindex
from clang.cindex import Config
from clang.cindex import Cursor
from clang.cindex import TypeKind
conf = Config()

# clang.cindex.SpellingCache has
# miss spelled value.
# http://llvm.org/bugs/show_bug.cgi?id=19089
clang.cindex.SpellingCache[8] = '['

# global variables around epc
server = EPCServer(('localhost', 0))

# global variables around libclang
index = None
tu = None


def make_cursor(file, line, col):
    """Return Cursor."""
    location = tu.get_location(file, (line, col))
    return Cursor.from_location(tu, location)


@server.register_function
def initialize(libclang_dir=None):
    """Initialize this server.

    Set additional library search path.
    libclang must exists at libclang_dir.
    Abd index is created.

    """
    global index
    if libclang_dir is not None:
        Config.set_library_path(libclang_dir)
    index = clang.cindex.Index.create()


def get_typeof_cursor_impl(cursor):
    """Return true typename.

    If variable at the cursor is typed auto, return inspected one.

    """
    if cursor.type.kind == TypeKind.UNEXPOSED:
        return get_typeof_cursor_impl(cursor.get_children().next())
    return cursor.type.spelling


@server.register_function
def get_typeof_cursor(file, line, col):
    """Return typename of the variable at the cursor."""
    cursor = make_cursor(file, line, col)
    return (cursor.type.spelling, cursor.type.get_canonical().spelling)


severity_map = ['ignored', 'note', 'warning', 'error', 'fatal error']


def source_location_to_tuple(source_location):
    """Return a tuple from source_location.

    Return a tuple formed (file_name, line, column)
    """
    return (source_location.file.name,
            source_location.line,
            source_location.column)


@server.register_function
def get_diagnostics():
    """Return a list of tuple diagnostics with fixits of translation unit."""
    result = []
    for diag in tu.diagnostics:
        diag_fixits = []
        for it in diag.fixits:
            range = it.range
            diag_fixits.append((source_location_to_tuple(range.start),
                                source_location_to_tuple(range.end),
                                it.value))
        location = diag.location
        file_name = "" if location.file is None else location.file.name
        result.append(((file_name,
                        location.line,
                        location.column,
                        severity_map[diag.severity],
                        diag.spelling),
                       diag_fixits))
    return result


def completion_result_to_tuple(cr):
    """Return tuple have informations of a candidate."""
    result_type = ""
    # typed_text = ""
    name_and_args = ""
    name_and_args_formed = ""
    arg_counter = 0
    cs = cr.string
    for chunk in cs:
        # if chunk.isKindTypedText():
        #     typed_text = chunk.spelling
        if chunk.isKindResultType():
            result_type = chunk.spelling
        else:
            name_and_args += chunk.spelling
            if chunk.isKindPlaceHolder():
                arg_counter += 1
                name_and_args_formed += "${{{}:{}}}".format(arg_counter,
                                                            chunk.spelling)
            elif not chunk.isKindInformative():
                name_and_args_formed += chunk.spelling
    brief_comment = cs.briefComment.spelling
    return (result_type,
            # typed_text,
            name_and_args,
            name_and_args_formed,
            brief_comment,
            cr.kind.value)


@server.register_function
def get_completions(file, line, column, buf_str, priority_limit=74):
    """Return completion candidates."""
    completions = tu.codeComplete(file,
                                  line,
                                  column,
                                  unsaved_files=[(file,
                                                  buf_str.encode('utf-8'))],
                                  include_brief_comments=True)
    result = []
    for x in completions.results:
        if x.string.priority > priority_limit:
            continue
        result.append(completion_result_to_tuple(x))
    return result


@server.register_function
def get_includes():
    """Return include header list."""
    result = []
    for it in tu.get_includes():
        result.append(it.source.name)
    return result

params = ["-std=c++11",
          "-Wall",
          "-Wextra",
          "-I/Users/kumar1/opt/llvm/include",
          "-I./picojson"]


@server.register_function
def reparse_unsaved_file(file, buf_str):
    """Re-parse buf_str at file."""
    global tu
    tu.reparse([(file, buf_str.encode('utf-8'))])


@server.register_function
def parse_unsaved_file(file, buf_str):
    """Parse buf_str at file."""
    global tu
    tu = index.parse(file,
                     unsaved_files=[(file, buf_str.encode('utf-8'))],
                     args=params,)

if __name__ == "__main__":
    # start server
    server.print_port()
    server.serve_forever()
