;;; flower-fixit.el --- Flower mode back-end for Fixit.

;; Author: kumar8600 <kumar8600@gmail.com>

;; Copyright (c) 2014 by kumar8600
;; All rights reserved.

;; Redistribution and use in source and binary forms, with or without modification,
;; are permitted provided that the following conditions are met:

;; * Redistributions of source code must retain the above copyright notice, this
;;   list of conditions and the following disclaimer.

;; * Redistributions in binary form must reproduce the above copyright notice, this
;;   list of conditions and the following disclaimer in the documentation and/or
;;   other materials provided with the distribution.

;; * Neither the name of the kumar8600 nor the names of its
;;   contributors may be used to endorse or promote products derived from
;;   this software without specific prior written permission.

;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
;; ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
;; DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
;; ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
;; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
;; ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
;; (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
;; SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

;;; Commentary:

;;; Code:

(require 'thingatpt)
(require 'popup)
(require 'flower-definitions)

(defun flower-fixit-location-file-name (location)
  "Return LOCATION's file-name."
  (car location))

(defun flower-fixit-location-line (location)
  "Return LOCATION's line number."
  (cadr location))

(defun flower-fixit-location-column (location)
  "Return LOCATION's column number."
  (- (caddr location) 1))

(defun flower-fixit-range-start (fixit)
  "Return FIXIT's start location of range."
  (car fixit))

(defun flower-fixit-range-end (fixit)
  "Return FIXIT's start location of end."
  (cadr fixit))

(defun flower-fixit-value (fixit)
  "Return FIXIT's value."
  (caddr fixit))

(defun flower-fixits-from-diagnostic (diagnostic)
  "Return fixit list is made from DIAGNOSTIC."
  (car-safe (cdr diagnostic)))

(defun flower-diagnostics-file-name-from-diagnostic (diagnostic)
  "Return a file name from DIAGNOSTIC."
  (caar diagnostic))

(defun flower-diagnostics-line-from-diagnostic (diagnostic)
  "Return a line from DIAGNOSTIC."
  (cadar diagnostic))

(defun flower-diagnostics-column-from-diagnostic (diagnostic)
  "Return a column from DIAGNOSTIC."
  (- (caddar diagnostic) 1))

(defun flower-diagnostics-severity-from-diagnostic (diagnostic)
  "Return a severity from DIAGNOSTIC."
  (cadddr (car diagnostic)))

(defun flower-diagnostics-body-from-diagnostic (diagnostic)
  "Return a body from DIAGNOSTIC."
  (nth 4 (car diagnostic)))

(defun flower-diagnostics-get-point-from-diagnostic (diagnostic)
  "Return point where DIAGNOSTIC."
  (flower-point-from-line-column (flower-diagnostics-line-from-diagnostic diagnostic)
				 (flower-diagnostics-column-from-diagnostic diagnostic)))

(defun flower-fixit-get-diagnostic-impl (point cand-point-init comp-item-param comp-cand-item)
  "Get a diagnostic from POINT.

Read below code.
CAND-POINT-INIT
COMP-ITEM-PARAM
COMP-CAND-ITEM."
  (let ((candidate nil)
	(candidate-point cand-point-init))
    (mapc (lambda (diag)
	    (let ((diag-point (flower-diagnostics-get-point-from-diagnostic diag)))
	      (when (and (funcall comp-item-param diag-point point)
			 (funcall comp-cand-item candidate-point diag-point))
		(setq candidate diag)
		(setq candidate-point diag-point))))
	  flower-diagnostics-current)
    candidate))

(defun flower-fixit-get-diagnostic-previous (point)
  "Get a previous diagnostic from POINT."
  (flower-fixit-get-diagnostic-impl point
				    1
				    #'<=
				    #'<))

(defun flower-fixit-get-diagnostic-next (point)
  "Get a next diagnostic from POINT."
  (flower-fixit-get-diagnostic-impl point
				    (point-max)
				    #'>=
				    #'>))

(defun flower-fixit-select-fixit (fixits)
  "Retun a selected fixit from a list FIXITS."
  (popup-menu*
   (mapcar (lambda (fixit)
	     (let ((range-start (flower-fixit-range-start fixit))
		   (range-end (flower-fixit-range-end fixit)))
	       (let ((start-point (flower-point-from-line-column
				   (flower-fixit-location-line range-start)
				   (flower-fixit-location-column range-start)))
		     (end-point   (flower-point-from-line-column
				   (flower-fixit-location-line range-end)
				   (flower-fixit-location-column range-end)))
		     (value (flower-fixit-value fixit)))
		 (let ((action (if (equal range-start
					  range-end)
				   (format "Insert `%s'."
					   value)
				 (format "Replace `%s' with `%s'."
					 (buffer-substring start-point
							   end-point)
					 value))))
		   (popup-make-item action
				    :value fixit
				    :summary "[Fix-it]")))))
	   fixits)))

(defun flower-fixit-from-diagnostic (diagnostic)
  "Do fixit get from DIAGNOSTIC."
  (let ((fixits (flower-fixits-from-diagnostic diagnostic)))
    (if fixits
	(progn
	  (let ((diagnostic-point (flower-diagnostics-get-point-from-diagnostic diagnostic))
		(fixit nil))
	    (goto-char diagnostic-point)
	    (let ((popup (popup-tip (concat
				     (flower-diagnostics-severity-from-diagnostic diagnostic)
				     ": "
				     (flower-diagnostics-body-from-diagnostic diagnostic))
				    :nowait t)))
	      (unwind-protect
		  (setq fixit (flower-fixit-select-fixit fixits))
		(popup-delete popup)))
	    (let* ((range-start (flower-fixit-range-start fixit))
		   (range-end (flower-fixit-range-end fixit))
		   (start-point (flower-point-from-line-column
				 (flower-fixit-location-line range-start)
				 (flower-fixit-location-column range-start)))
		   (end-point   (flower-point-from-line-column
				 (flower-fixit-location-line range-end)
				 (flower-fixit-location-column range-end)))
		   (value (flower-fixit-value fixit)))
	      (save-excursion
		(delete-region start-point
			       end-point)
		(goto-char start-point)
		(insert value)))))
      (message "There are no fixits.")
      nil)))

(defun flower-fixit-previous ()
  "Do previous fixit from current pointer."
  (interactive)
  (flower-fixit-from-diagnostic (flower-fixit-get-diagnostic-previous
		      (or (cdr-safe (bounds-of-thing-at-point 'symbol))
			  (point)))))

(defun flower-fixit-next ()
  "Do previous fixit from current pointer."
  (interactive)
  (flower-fixit-from-diagnostic (flower-fixit-get-diagnostic-next
		      (or (car-safe (bounds-of-thing-at-point 'symbol))
			  (point)))))

(provide 'flower-fixit)

;;; flower-fixit.el ends here
