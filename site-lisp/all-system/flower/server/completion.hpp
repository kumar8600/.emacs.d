#include <string>
#include <clang-c/Index.h>
#include <picojson.h>

class Completion {
  CXCodeCompleteResults * CodeCompleteAt(CXTranslationUnit & tu, const char * file_name, unsigned line, unsigned column, const char * unsaved_str);
  void PrintCompleteDiagnostics(CXCodeCompleteResults * res);
  picojson::value GetCandidates(CXCodeCompleteResults * res, unsigned priority_limit = 74);

 public:
  static const std::string GetCommandName(void)
  {
    return std::string("complete");
  };
  
  static picojson::value Execute(CXTranslationUnit & tu, const picojson::value & arg);
};
