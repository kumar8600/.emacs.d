#include <cstdlib>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <clang-c/Index.h>
#include <picojson.h>
#include "completion.hpp"

const char * unsaved_str = "#include <cstdio>\n\
#include <vector>\n\
#include <OpenGL/gl3.h>\n\
\n\
int plus(int a, int b) {\n\
  return a + b;\n\
}\n\
\n\
namespace hoge{\n\
/// minus A and B.\n\
int minus(int a, int b) {\n\
  return a - b;\n\
}\n\
}\n\
\n\
\n\
template<typename T>\n\
struct X{};\n\
template<> struct X<int>{};\n\
\n\
int main(){\n\
  std::vector<int> p;\n\
  using hoge::minus;\n\
  \n\
  hoge::minus(1, 2);\n\
  p.push_back(1);\n\
  hoge::minus(1, 2);\n\
  p.push_back(1);\n\
  typedef int うんこ;\n\
  int tin;\n\
\n\
int a = 1; int b = 2;\n\
  auto unko = 1.0; うんこ ちんこ; ちんこ; ちんこ; plus(unko, 2); minus(3, 5);\n\
  return 0;\n\
}\n";

int main(int argc, char *argv[])
{
  if (argc < 4) {
    std::cout << argv[0] << " file_path line column [clang args...]"
              << std::endl;
    return 1;
  }

  // Create index
  CXIndex idx = clang_createIndex(1, 0);
  if (!idx) {
    std::cerr << "createIndex failed" << std::endl;
    return 2;
  }

  // Parse
  CXTranslationUnit tu =
      clang_parseTranslationUnit(idx,
                                 argv[1],
                                 argv + 4,
                                 argc - 4,
                                 nullptr,
                                 0,
                                 CXTranslationUnit_PrecompiledPreamble |
                                 CXTranslationUnit_CacheCompletionResults);
  if (!tu) {
    std::cerr << "parseTranslationUnit failed" << std::endl;
    return 2;
  }

  // Complete
  unsigned line = std::strtoul(argv[2], nullptr, 10);
  unsigned column = std::strtoul(argv[3], nullptr, 10);

  std::vector<picojson::value> arg {
    picojson::value("complete"),
        picojson::value(static_cast<double>(line)),
        picojson::value(static_cast<double>(column)),
        picojson::value(unsaved_str)
        };
  picojson::value v_arg(std::move(arg));

  std::cout << Completion::Execute(tu, v_arg);
  
  
  // Clean up
  clang_disposeTranslationUnit(tu);  
  
  return 0;
}
