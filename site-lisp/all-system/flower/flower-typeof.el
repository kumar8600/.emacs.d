;;; flower-typeof.el --- Flower mode back-end for Typeof

;; Author: kumar8600 <kumar8600@gmail.com>

;; Copyright (c) 2014 by kumar8600
;; All rights reserved.

;; Redistribution and use in source and binary forms, with or without modification,
;; are permitted provided that the following conditions are met:

;; * Redistributions of source code must retain the above copyright notice, this
;;   list of conditions and the following disclaimer.

;; * Redistributions in binary form must reproduce the above copyright notice, this
;;   list of conditions and the following disclaimer in the documentation and/or
;;   other materials provided with the distribution.

;; * Neither the name of the kumar8600 nor the names of its
;;   contributors may be used to endorse or promote products derived from
;;   this software without specific prior written permission.

;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
;; ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
;; DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
;; ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
;; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
;; ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
;; (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
;; SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

;;; Commentary:

;;; Code:

(require 'epc)
(require 'flower-definitions)
(require 'flower-parse)
(require 'flower-timer)

(defun flower-typeof ()
  "Show type name of variable at cursor position."
  (interactive)
  (flower-pre-check)
  (flower-with-reparse
   (lambda (result)
     (let ((lin (flower-current-line-number))
	   (col (flower-current-column-number)))
       (deferred:$
	 (epc:call-deferred flower-epc 'get_typeof_cursor (list (buffer-file-name)
								lin
								col))
	 (deferred:nextc it
	   (lambda (result)
	     (message "type      : %s\ncanonical : %s"
		      (car result)
		      (cadr result)))))))))

(defun flower-enable-auto-typeof ()
  "Enable auto typeof echoing."
  (interactive)
  (add-hook 'flower-idle-hook #'flower-typeof))

(defun flower-disable-auto-typeof ()
  "Disable auto typeof echoing."
  (interactive)
  (remove-hook 'flower-idle-hook #'flower-typeof))

(defun flower-toggle-auto-typeof ()
  "Toggle auto typeof echoing."
  (interactive)
  (if (cl-find #'flower-echo-typeof
	       flower-idle-hook)
      (flower-disable-auto-typeof)
    (flower-enable-auto-typeof)))

(provide 'flower-typeof)

;;; flower-typeof.el ends here
