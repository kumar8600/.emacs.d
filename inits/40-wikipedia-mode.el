;;; 40-wikipedia-mode.el --- Wikipedia mode configuration for emacs.

;; Author: kumar8600 <kumar8600@gmail.com>

;; Copyright (c) 2014 by kumar8600
;; All rights reserved.

;;; Commentary:

;;; Code:

(require 'wikipedia-mode)

(add-to-list 'auto-mode-alist
    '("\\.wiki\\'" . wikipedia-mode))

;;; 40-wikipedia-mode.el ends here
