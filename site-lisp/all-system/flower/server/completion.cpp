#include "completion.hpp"
#include <clang-c/Index.h>
#include <picojson.h>
#include <sstream>
#include <boost/scope_exit.hpp>

CXCodeCompleteResults * Completion::CodeCompleteAt(CXTranslationUnit & tu, const char * file_name, unsigned line, unsigned column, const char * unsaved_str) {
  CXUnsavedFile unsaved_file;
  unsaved_file.Filename = file_name;
  unsaved_file.Contents = unsaved_str;
  unsaved_file.Length = std::strlen(unsaved_file.Contents);
  
  CXCodeCompleteResults * res =
      clang_codeCompleteAt(tu,
                           file_name,
                           line,
                           column,
                           &unsaved_file,
                           1,
                           clang_defaultCodeCompleteOptions() |
                           CXCodeComplete_IncludeBriefComments);
  if (!res) {
    std::cerr << "codeCompleteAt failed" << std::endl;
    std::exit(2);
  }
  return res;
}

void Completion::PrintCompleteDiagnostics(CXCodeCompleteResults * res) {
  for (unsigned i = 0; i < clang_codeCompleteGetNumDiagnostics(res); ++i) {
    const CXDiagnostic & diag = clang_codeCompleteGetDiagnostic(res, i);
    const CXString & s = clang_getDiagnosticSpelling(diag);
    std::cout << clang_getCString(s) << std::endl;
  }
}

picojson::value Completion::GetCandidates(CXCodeCompleteResults * res, unsigned priority_limit) {
  std::vector<picojson::value> candidates;
  candidates.reserve(res->NumResults);
  for (unsigned i = 0; i < res->NumResults; i++) {
    const CXCompletionString & str = res->Results[i].CompletionString;
    if (priority_limit < clang_getCompletionPriority(str))
      continue;
    
    unsigned incomplete_flag;
    CXCursorKind container_kind = clang_codeCompleteGetContainerKind(res, &incomplete_flag);  
    
    const char * result_type;
    std::stringstream name_and_args;
    std::stringstream name_and_args_formed;
    const char * brief = "";
    CXString cx_brief = clang_getCompletionBriefComment(str);
    if (cx_brief.data) {
      brief = clang_getCString(cx_brief);
    }
    int arg_counter = 0;
 
    for (unsigned j = 0; j < clang_getNumCompletionChunks(str); j++) {
      CXCompletionChunkKind kind = clang_getCompletionChunkKind(str, j);
      const char * text = clang_getCString(clang_getCompletionChunkText(str, j));
      if (kind == CXCompletionChunk_ResultType) {
        result_type = text;
      } else {
        name_and_args << text;
        if (kind == CXCompletionChunk_Placeholder) {
          ++arg_counter;
          name_and_args_formed << "${" << arg_counter << ":" << text << "}";
        } else if (kind != CXCompletionChunk_Informative) {
          name_and_args_formed << text;
        }
      }
    }
    std::vector<picojson::value> chunks {
      picojson::value(result_type),
          picojson::value(name_and_args.str()),
          picojson::value(name_and_args_formed.str()),
          picojson::value(static_cast<double>(container_kind)),
          picojson::value(brief)
    };
    picojson::value v_chunks(std::move(chunks));
    candidates.push_back(std::move(v_chunks));
  }
  return picojson::value(std::move(candidates));
}

picojson::value Completion::Execute(CXTranslationUnit & tu, const picojson::value & arg) {
  unsigned line = arg.get(2).get<unsigned>();
  unsigned column = arg.get(3).get<unsigned>();
  CXCodeCompleteResults * res = CodeCompleteAt(
      tu,
      arg.get(1).get<std::string>().c_str(),
      line,
      column,
      arg.get(4).get(4).get<std::string>().c_str());
  BOOST_SCOPE_EXIT((res))
  {
    // Cleanup
    clang_disposeCodeCompleteResults(res);
  }
  BOOST_SCOPE_EXIT_END;
  
  // Print candidates
  return GetCandidates(res);
}
