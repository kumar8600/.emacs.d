# -*- coding: utf-8 -*-

import sys
import timeit
import clang.cindex
from clang.cindex import Config
import sexpdata

index = None
tu = None

unsaved_src = """
#include <cstdio>
#include <vector>
#include <OpenGL/gl3.h>

int plus(int a, int b) {
  return a + b;
}

namespace hoge{
/// minus A and B.
int minus(int a, int b) {
  return a - b;
}
}


template<typename T>
struct X{};
template<> struct X<int>{};

int main(){
  std::vector<int> p;
  p.push_back(1);

  hoge::minus(1, 2);
  p.push_back(1);
  hoge::minus(1, 2);
  p.push_back(1);
  typedef int うんこ;
  using hoge::minus;
  int tin;

int a = 1; int b = 2;
  auto unko = 1.0; うんこ ちんこ; ちんこ; ちんこ; plus(unko, 2); minus(3, 5);
  return 0;
}
"""


def initialize(file_path, libclang_dir=None):
    """Initialize this server.

    Set additional library search path.
    libclang must exists at libclang_dir.
    Abd index is created.

    """
    global index
    if libclang_dir is not None:
        Config.set_library_path(libclang_dir)
    index = clang.cindex.Index.create()
    global tu
    tu = index.parse(file_path)


def completion_result_to_tuple(cr):
    """Return tuple have informations of a candidate."""
    result_type = ""
    # typed_text = ""
    name_and_args = ""
    name_and_args_formed = ""
    arg_counter = 0
    cs = cr.string
    for chunk in cs:
        # if chunk.isKindTypedText():
        #     typed_text = chunk.spelling
        if chunk.isKindResultType():
            result_type = chunk.spelling
        else:
            name_and_args += chunk.spelling
            if chunk.isKindPlaceHolder():
                arg_counter += 1
                name_and_args_formed += "${{{}:{}}}".format(arg_counter,
                                                            chunk.spelling)
            elif not chunk.isKindInformative():
                name_and_args_formed += chunk.spelling
    brief_comment = cs.briefComment.spelling
    return (result_type,
            # typed_text,
            name_and_args,
            name_and_args_formed,
            brief_comment,
            cr.kind.value)


def print_completions(file_path, line, column, unsaved_str,
                      priority_limit=74):
    """Print completion candidates."""
    completions = tu.codeComplete(file_path,
                                  line,
                                  column,
                                  unsaved_files=[(file_path, unsaved_str)],
                                  include_brief_comments=True)
    for x in completions.results:
        if x.string.priority > priority_limit:
            continue
        # sexpdata.dump(completion_result_to_tuple(x), sys.stdout)
        # print("")

if __name__ == "__main__":
    # start server
    if len(sys.argv) < 4:
        print("Arguments are too few!")
        print("format: <file_path> <line> <column>")
    initialize(sys.argv[1], "/Users/kumar1/opt/llvm/lib")
    t = timeit.Timer(lambda: print_completions(sys.argv[1],int(sys.argv[2]),int(sys.argv[3]), unsaved_src))
    print(t.timeit(number=1))
