;;; 30-snippet.el --- My emacs configuration for snippet packages.

;; Author: kumar8600 <kumar8600@gmail.com>

;; Copyright (c) 2014 by kumar8600
;; All rights reserved.

;;; Commentary:

;;; Code:

(require 'yasnippet)

(yas-global-mode 1)

;;; 30-snippet.el ends here
