;;; 25-helm.el --- My emacs configurations for Helm.

;; Author: kumar8600 <kumar8600@gmail.com>

;; Copyright (c) 2014 by kumar8600
;; All rights reserved.

;;; Commentary:

;;; Code:

(require 'helm-config)

(add-hook 'after-init-hook (lambda () (helm-mode 1)))

;;; 25-helm.el ends here
