;;; flower-completion.el --- Flower mode back-end for code completion.

;; Author: kumar8600 <kumar8600@gmail.com>

;; Copyright (c) 2014 by kumar8600
;; All rights reserved.

;; Redistribution and use in source and binary forms, with or without modification,
;; are permitted provided that the following conditions are met:

;; * Redistributions of source code must retain the above copyright notice, this
;;   list of conditions and the following disclaimer.

;; * Redistributions in binary form must reproduce the above copyright notice, this
;;   list of conditions and the following disclaimer in the documentation and/or
;;   other materials provided with the distribution.

;; * Neither the name of the kumar8600 nor the names of its
;;   contributors may be used to endorse or promote products derived from
;;   this software without specific prior written permission.

;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
;; ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
;; DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
;; ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
;; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
;; ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
;; (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
;; SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

;;; Commentary:

;; Candidates list returned by server is list of:

; (result_type
;  name_and_args
;  name_and_args_formed
;  brief_comment)

;;; Code:

(require 'epc)
(require 'popup)
(require 'auto-complete)
(require 'yasnippet)
(require 'flower-definitions)
(require 'flower-parse)

;;;; Interface
(defvar-local flower-completion nil
  "Non-nil if completion is enabled.")

(defvar-local flower-ac-auto-start-bkup nil
  "Non-nil means completion will be started automatically.

It is set by `flower-enable-completion' automatically.")

(defun flower-enable-completion ()
  "Enable code completion."
  (if ac-auto-start
      (progn
	(flower-enable-async-completion)
	(setq flower-completion 'async))
    (progn
      (flower-enable-sync-completion)
      (setq flower-completion 'sync)))
  ;;;; No need to delay because async.
  (when ac-auto-show-menu
    (setq-local ac-auto-show-menu t))
  (setq-local ac-delay 0))

(defun flower-disable-completion ()
  "Disable code completion."
  (case flower-completion
    ('sync  (flower-disable-sync-completion))
    ('async (flower-disable-async-completion)))
  (setq-local flower-completion nil))

;;;; Sync / Async completion interface
(defun flower-enable-sync-completion ()
  "Enable synchronous code completion."
  (add-to-list 'ac-sources 'ac-source-flower-sync)
  (add-hook 'post-self-insert-hook #'flower-async-completion-start nil t))

(defun flower-disable-sync-completion ()
  "Enable asynchronous code completion."
  (remove-hook 'post-self-insert-hook #'flower-async-completion-start t)
  (delq 'ac-source-flower-sync ac-sources)
  (flower-async-completion-clean))
 
(defun flower-enable-async-completion ()
  "Enable asynchronous code completion."
  (add-to-list 'ac-sources 'ac-source-flower-async)
  (add-hook 'post-self-insert-hook #'flower-async-completion-start nil t)
  (setq-local flower-ac-auto-start-bkup ac-auto-start)
  (setq-local ac-auto-start nil))

(defun flower-disable-async-completion ()
  "Disable asynchronous code completion."
  (remove-hook 'post-self-insert-hook #'flower-async-completion-start t)
  (delq 'ac-source-flower-async ac-sources)
  (setq-local ac-auto-start flower-ac-auto-start-bkup)
  (flower-async-completion-clean))

;;;; EPC transactions
(defun flower-get-candidates-async (line column finish-func)
  "Get code completion candidates at LINE COLUMN and pass to FINISH-FUNC."
  (deferred:$
    (epc:call-deferred flower-epc 'get_completions (list (buffer-file-name)
							 line
							 column
							 (buffer-substring-no-properties
							  (point-min) (point-max))))
    (deferred:nextc it
      finish-func)))

;;;; Convert
  
(defun flower-make-ac-candidate (item)
  "Make ITEM candidate item."
  (let ((name-and-args (cadr item))
	(name-and-args-formed (caddr item))
	(brief-comment (cadddr item))
	(return-type (car item))
	(type (nth 4 item)))
    (popup-make-item name-and-args
		     :value name-and-args-formed
		     :document brief-comment
		     :summary (unless (string= "" return-type)
				(format "[%s]" return-type))
		     :symbol (flower-kind-id-to-ac-symbol type))))

;;;; Auto Complete control
(defvar-local flower-ac-async-candidates-list nil
  "This is implementation variable describe candidates.")

(defvar-local flower-ac-last-prefix-point nil
  "This is variable describes prefix's point last used.")

(defvar-local flower-ac-async-updating-flag nil
  "Non-nil means updating candidates.")

(defvar-local flower-ac-async-update-again-immediately-flag nil
  "Non-nil if candidates are need to update again immediately.")

;;;; Auto Complete synchronous control
(defun flower-ac-sync-candidates ()
  "Return candidates list for AC.

Note: it is synchronous function.  But back-end is not.
This function wait for end of candidates updating."
  (flower-future-wait 'flower-ac-async-updating-flag 10)
  flower-ac-async-candidates-list)

;;;; Auto Complete asynchronous control
(defun flower-async-completion-clean ()
  "Clean variables for async completion."
  (setq-local flower-ac-async-candidates-list nil)
  (setq-local flower-ac-last-prefix-point nil)
  (setq-local flower-ac-async-updating-flag nil)
  (setq-local flower-ac-async-update-again-immediately-flag nil))

(defun flower-ac-async-candidates ()
  "Return candidates list for AC.

If candidates are asynchronous updating, return fake."
  (if flower-ac-async-updating-flag
      (let ((prefix (buffer-substring-no-properties (flower-completion-prefix)
						  (point))))
	(list prefix))
    flower-ac-async-candidates-list))

(defun flower-get-candidates-ac-async-point (finish-func)
  "Create candidates for ac and pass to FINISH-FUNC."
  (let ((lc (flower-point-to-line-column (flower-completion-prefix))))
    (flower-get-candidates-async (car lc)
				 (cdr lc)
				 finish-func)))
(defun flower-ac-async-update ()
  "When client typed something, this function is called.

When the time to start ac, transact with server and show candidates."
  (if flower-ac-async-updating-flag
      (setq flower-ac-async-update-again-immediately-flag t)
    (setq flower-ac-async-updating-flag t)
    (lexical-let ((buffer (current-buffer)))
      (flower-get-candidates-ac-async-point
       (lambda (result)
	 (message "got")
	 (if flower-ac-async-update-again-immediately-flag
	     (flower-async-completion-clean)
	     (when (equal buffer (current-buffer))
	       (flower-async-completion-start))
	   (unwind-protect
	       (when (equal buffer (current-buffer))
		 (setq flower-ac-async-candidates-list
		       (mapcar #'flower-make-ac-candidate
			       result)))
	     (setq flower-ac-async-updating-flag nil))
	   ;; NOTICE: Below couple of functions are not documented.
	   ;;         There is a risk they will be removed by AC project.
	   (when (eq flower-completion 'async)
	     (ac-start :force-init t)
	     (ac-update))))))))

(defun flower-async-completion-start ()
  "Start completion."
  (interactive)
  (when (and (ignore-errors
	       (flower-pre-check)
	       t)
	     auto-complete-mode)
    (let ((prefix-point (flower-completion-prefix)))
      (when prefix-point
	(if (not (equal flower-ac-last-prefix-point
			prefix-point))
	    (progn
	      (setq flower-ac-last-prefix-point prefix-point)
	      ;; FIX ME: When call `ac-start' firstly in async, not focused to AC.
	      ;;         So, I begin AC here synchronously.
	      (when (eq flower-completion 'async)
		(ac-start))
	      (flower-ac-async-update))
	  (when (eq flower-completion 'async)
	    (when (and (not ac-completing)
		       (not flower-ac-async-updating-flag))
	      (ac-start))))))))

;;;; Auto Complete configurations
(defconst flower-ac-start-prefixes
  '("."
    "->"
    "::")
  "When one of them is typed, start completion.")

(defun flower-ac-start-prefix-before-pointer-p ()
  "Return non-nil if there is one of the prefix before pointer."
  (cl-find-if (lambda (item)
		(string= item
			 (ignore-errors
			   (buffer-substring-no-properties
			    (- (point) (length item))
			    (point)))))
	      flower-ac-start-prefixes))

(defun flower-completion-prefix ()
  "Pass prefix point for auto complete."
  (let ((prefix-point (or (ac-prefix-symbol)
			  (when (flower-ac-start-prefix-before-pointer-p)
			    (point)))))
    (when prefix-point
      (let ((prefix (buffer-substring-no-properties prefix-point (point))))
	(if (or (string= "" prefix)
		(string-match "^[^0-9]" prefix))
	    prefix-point
	  nil)))))

(defun flower-ac-async-prefix ()
  "Pass prefix point for auto complete.

If need to re-generate candidates list, do it."
  (let ((p (flower-completion-prefix)))
    (when (and p
	       (not (equal flower-ac-last-prefix-point
			   p)))
      (flower-async-completion-start))
    p))

(defun flower-expand-snippet (snippet &optional start end)
  "Expand SNIPPET.

Text between START and END will be deleted before inserting."
  (if (featurep 'yasnippet)
      (yas-expand-snippet snippet start end)
    (error "Yasnippet is not loaded:  Please install Yasnippet")))

(defun flower-ac-action ()
  "Process candidate is selected."
  (let* ((selected (cdr ac-last-completion))
	 (snippet (get-text-property 0 'value selected)))
    (when snippet
      (yas-expand-snippet snippet
			  (- (point) (length selected))
			  (point)))))

(ac-define-source flower-sync
  '((candidates . flower-ac-sync-candidates)
    (prefix . flower-completion-prefix)
    (action . flower-ac-action)
    (requires . 0)
    (limit . nil)
    (cache)))

(ac-define-source flower-async
  '((candidates . flower-ac-async-candidates)
    (prefix . flower-ac-async-prefix)
    (action . flower-ac-action)
    (requires . 0)
    (limit . nil)))

(provide 'flower-completion)

;;; flower-completion.el ends here
