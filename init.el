;;; init.el --- My emacs configurations.

;; Author: kumar8600 <kumar8600@gmail.com>

;; Copyright (c) 2014 by kumar8600
;; All rights reserved.

;;; Commentary:

;;; Code:

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes (quote ("756597b162f1be60a12dbd52bab71d40d6a2845a3e3c2584c6573ee9c332a66e" default)))
 '(flycheck-php-phpcs-executable "true"))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(flycheck-color-mode-line-error-face ((t :foreground "#efefef" :background "#ed7676" :weight normal :inherit flycheck-fringe-error)))
 '(flycheck-color-mode-line-info-face ((t :foreground "#efefef" :background "#75a4ea" :weight normal :inherit flycheck-fringe-warning)))
 '(flycheck-color-mode-line-warning-face ((t :foreground "#efefef" :background "#eaea75" :weight normal :inherit flycheck-fringe-warning)))
 '(linum-highlight-face ((t (:foreground "orange3")))))

;; ガベージコレクションの間隔を広げる
(setq gc-cons-threshold 134217728)

;; スタートアップページを表示しない
(setq inhibit-startup-message t)

;;
;; load-path の設定
;;

(defun set-load-path-deep (top-dir)
  "Set `load-path' recursively from TOP-DIR."
  (let ((default-directory (expand-file-name top-dir)))
    (add-to-list 'load-path default-directory)
    (if (fboundp 'normal-top-level-add-subdirs-to-load-path)
	(normal-top-level-add-subdirs-to-load-path))))

;; ~/.emacs.d/site-lisp/all-system 以下すべて読み込み
(set-load-path-deep "~/.emacs.d/site-lisp/all-system")

;; プラットフォーム毎に異なるパスを設定する
;; windows: windows-nt, linux: gnu-linux, mac os x: darwin
(let ((platform-dir (concat "~/.emacs.d/site-lisp/"
			    (replace-regexp-in-string "/"
						      "-"
						      (symbol-name system-type)))))
  (when (file-exists-p platform-dir)
    (set-load-path-deep platform-dir)))

;; Mac の場合のパスも追加
(if (eq system-type
	'darwin)
    (add-to-list 'load-path "/usr/local/share/emacs/site-lisp/"))

;; inits ディレクトリをロードパスに追加
(add-to-list 'load-path "~/.emacs.d/inits")

;; ;; inits ディレクトリの中の.elファイルを、必要が有らばバイトコンパイル
;; (byte-recompile-directory "~/.emacs.d/inits")

;;
;; inits を読み込む
;;
(defvar my-inits
  '("01-basic-configurations"
    "10-package"
    "20-appearance"
    "25-helm"
    "30-buffer"
    "30-key-act"
    "30-run"
    "30-snippet"
    "30-completion"
    "30-checker"
    "30-vcs"
    "40-org-mode"
    "40-slime"
    "40-cpp-mode"
    "40-python-mode"
    "40-wikipedia-mode"
    "45-window-system"
    "50-user-interface"
    "60-twitter.el"
    ))

;; (mapc (lambda (item)
;; 	(load-file (concat "~/.emacs.d/inits/" item ".el")))
;;       my-inits)

(mapc #'load
      my-inits)

;;; init.el ends here
