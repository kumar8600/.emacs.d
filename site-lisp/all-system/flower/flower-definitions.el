;;; flower-definitions.el --- Flower mode back-end for core variables.

;; Author: kumar8600 <kumar8600@gmail.com>

;; Copyright (c) 2014 by kumar8600
;; All rights reserved.

;; Redistribution and use in source and binary forms, with or without modification,
;; are permitted provided that the following conditions are met:

;; * Redistributions of source code must retain the above copyright notice, this
;;   list of conditions and the following disclaimer.

;; * Redistributions in binary form must reproduce the above copyright notice, this
;;   list of conditions and the following disclaimer in the documentation and/or
;;   other materials provided with the distribution.

;; * Neither the name of the kumar8600 nor the names of its
;;   contributors may be used to endorse or promote products derived from
;;   this software without specific prior written permission.

;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
;; ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
;; DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
;; ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
;; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
;; ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
;; (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
;; SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

;;; Commentary:

;;; Code:

(require 'epc)

;;;; User customizable variables
(defmacro flower-def-option-var (symbol value doc-string &rest rest)
  "Define custom variable grouped in yabai-options.

Variable SYMBOL with VALUE documented with DOC-STRING and REST for defcustom."
  `(defcustom ,symbol ,value
     ,doc-string
     :group 'flower-options
     ,@rest))

(flower-def-option-var flower-libclang-directory-path nil
		       "Additional library search path to load libclang.

If you changed this option, please restart flower-mode or emacs."
		       :type 'string)

;;;; Variables for Implementations
(defvar flower-mode) ;; This is dummy. FIX ME

(defvar-local flower-epc nil
  "EPC handle.")

(defvar-local flower-need-to-reparse-flag nil
  "Non-nil means libclang's reparsing is needed.")

(defvar-local flower-diagnostics-current nil
  "A list of current diagnostics.")

;;;; Utilities
(defun flower-current-column-bytes ()
  "Return current column number count with bytes."
  (string-bytes (buffer-substring-no-properties (line-beginning-position)
						(point))))

(defun flower-current-line-number ()
  "Return current line number."
  (line-number-at-pos))

(defun flower-current-column-number ()
  "Return current column number.

Trace for clang's working, plus one for result."
  (1+ (flower-current-column-bytes)))

(defun flower-point-to-line-column (point)
  "Return cons of line and column of POINT."
  (save-excursion
    (goto-char point)
    (cons (flower-current-line-number)
	  (flower-current-column-number))))

(defun flower-point-from-line-column (line column)
  "Return point at LINE and COLUMN."
  (save-excursion
    (goto-char (point-min))
    (forward-line (- line 1))
    (move-to-column column)
    (point)))

(defun flower-future-wait (var-symbol &optional limit-sec)
  "Wait till VAR-SYMBOL becomes nil.

Optionally, if LIMIT-SEC sec is progressed, throw `wait-error'.
Return progressed time."
  (let ((progressed 0.0))
    (while (eval var-symbol)
      (sleep-for 0.05)
      (accept-process-output)
      (cl-incf progressed 0.05)
      (when (< limit-sec progressed)
	(error 'wait-error)))
    progressed))

(defun flower-pre-check ()
  "Check before do something."
  (unless flower-mode
    (error "Flower Mode is not enabled"))
  (unless (epc:live-p flower-epc)
    (error "Server is not working")))

;;;; Libclang cursor type name maps
(defconst flower-kind-id-name-map
  [""					; 0
   "UNEXPOSED_DECL"			; 1
   "STRUCT_DECL"			; 2
   "UNION_DECL"				; 3
   "CLASS_DECL"				; 4
   "ENUM_DECL"				; 5
   "FIELD_DECL"				; 6
   "ENUM_CONSTANT_DECL"			; 7
   "FUNCTION_DECL"			; 8
   "VAR_DECL"				; 9
   "PARM_DECL"				; 10
   "OBJC_INTERFACE_DECL"		; 11
   "OBJC_CATEGORY_DECL"			; 12
   "OBJC_PROTOCOL_DECL"			; 13
   "OBJC_PROPERTY_DECL"			; 14
   "OBJC_IVAR_DECL"			; 15
   "OBJC_INSTANCE_METHOD_DECL"		; 16
   "OBJC_CLASS_METHOD_DECL"		; 17
   "OBJC_IMPLEMENTATION_DECL"		; 18
   "OBJC_CATEGORY_IMPL_DECL"		; 19
   "TYPEDEF_DECL"			; 20
   "CXX_METHOD"				; 21
   "NAMESPACE"				; 22
   "LINKAGE_SPEC"			; 23
   "CONSTRUCTOR"			; 24
   "DESTRUCTOR"				; 25
   "CONVERSION_FUNCTION"		; 26
   "TEMPLATE_TYPE_PARAMETER"		; 27
   "TEMPLATE_NON_TYPE_PARAMETER"	; 28
   "TEMPLATE_TEMPLATE_PARAMETER"	; 29
   "FUNCTION_TEMPLATE"			; 30
   "CLASS_TEMPLATE"			; 31
   "CLASS_TEMPLATE_PARTIAL_SPECIALIZATION" ; 32
   "NAMESPACE_ALIAS"			   ; 33
   "USING_DIRECTIVE"			   ; 34
   "USING_DECLARATION"			   ; 35
   "TYPE_ALIAS_DECL"			   ; 36
   "OBJC_SYNTHESIZE_DECL"		   ; 37
   "OBJC_DYNAMIC_DECL"			   ; 38
   "CXX_ACCESS_SPEC_DECL"		   ; 39
   ])

(defconst flower-kind-id-description-map
  ["Not specified."	                ; 0
   "A unexposed declaration."		; 1  : UNEXPOSED_DECL
   "A C or C++ struct."			; 2  : STRUCT_DECL
   "A C or C++ union."			; 3  : UNION_DECL
   "A C++ class."			; 4  : CLASS_DECL
   "An enumeration."			; 5  : ENUM_DECL
   "A field (in C) or non-static data member (in C++) in a struct, union, or C++ class." ; 6  : FIELD_DECL
   "An enumerator constant."	        ; 7  : ENUM_CONSTANT_DECL
   "A function."			; 8  : FUNCTION_DECL
   "A variable."			; 9  : VAR_DECL
   "A function or method parameter."	; 10 : PARM_DECL
   "An Objective-C @interface."		; 11 : OBJC_INTERFACE_DECL
   "An Objective-C @interface for a category." ; 12 : OBJC_CATEGORY_DECL
   "An Objective-C @protocol declaration." ; 13 : OBJC_PROTOCOL_DECL
   "An Objective-C @property declaration." ; 14 : OBJC_PROPERTY_DECL
   "An Objective-C instance variable."	   ; 15 : OBJC_IVAR_DECL
   "An Objective-C instance method."  ; 16 : OBJC_INSTANCE_METHOD_DECL
   "An Objective-C class method."     ; 17 : OBJC_CLASS_METHOD_DECL
   "An Objective-C @implementation."  ; 18 : OBJC_IMPLEMENTATION_DECL
   "An Objective-C @implementation for a category." ; 19 : OBJC_CATEGORY_IMPL_DECL
   "A typedef."				  ; 20 : TYPEDEF_DECL
   "A C++ class method."		  ; 21 : CXX_METHOD
   "A C++ namespace."			  ; 22 : NAMESPACE
   "A linkage specification, e.g. 'extern "C"'." ; 23 : LINKAGE_SPEC
   "A C++ constructor."				 ; 24 : CONSTRUCTOR
   "A C++ destructor."				 ; 25 : DESTRUCTOR
   "A C++ conversion function."	    ; 26 : CONVERSION_FUNCTION
   "A C++ template type parameter"  ; 27 : TEMPLATE_TYPE_PARAMETER
   "A C++ non-type template paramater."	; 28 : TEMPLATE_NON_TYPE_PARAMETER
   "A C++ template template parameter."	; 29 : TEMPLATE_TEMPLATE_PARAMETER
   "A C++ function template."		; 30 : FUNCTION_TEMPLATE
   "A C++ class template."		; 31 : CLASS_TEMPLATE
   "A C++ class template partial specialization." ; 32 : CLASS_TEMPLATE_PARTIAL_SPECIALIZATION
   "A C++ namespace alias declaration."	; 33 : NAMESPACE_ALIAS
   "A C++ using directive."		; 34 : USING_DIRECTIVE
   "A C++ using declaration."		; 35 : USING_DECLARATION
   "A Type alias decl."			; 36 : TYPE_ALIAS_DECL
   "A Objective-C synthesize decl."	; 37 : OBJC_SYNTHESIZE_DECL
   "A Objective-C dynamic decl."	; 38 : OBJC_DYNAMIC_DECL
   "A C++ access specifier decl."	; 39 : CXX_ACCESS_SPEC_DECL
   ])

;; Symbol for AC
;;   They are printed for each candidates.
;; AC official symbols:
;; |--------+------------------|
;; | Symbol | Meaning          |
;; |--------+------------------|
;; | s      | Symbol           |
;; | f      | Function, Method |
;; | v      | Variable         |
;; | c      | Constant         |
;; | a      | Abbreviation     |
;; | d      | Dictionary       |
;; |--------+------------------|
;; My additional symbols:
;; |--------+----------------------------|
;; | Symbol | Meaning                    |
;; |--------+----------------------------|
;; | -      | Unexposed, Not implemented |
;; | S      | Struct                     |
;; | U      | Union                      |
;; | C      | Class                      |
;; | E      | Enum                       |
;; | t      | Typedef                    |
;; | N      | Namespace                  |
;; |--------+----------------------------|
;; FIX ME: I don't know about Objective C.

(defconst flower-kind-id-ac-symbol-map
  ["-"					; 0
   "-"					; 1  : UNEXPOSED_DECL
   "S"					; 2  : STRUCT_DECL
   "U"					; 3  : UNION_DECL
   "C"					; 4  : CLASS_DECL
   "E"					; 5  : ENUM_DECL
   "v"					; 6  : FIELD_DECL
   "c"					; 7  : ENUM_CONSTANT_DECL
   "f"					; 8  : FUNCTION_DECL
   "v"					; 9  : VAR_DECL
   "v"					; 10 : PARM_DECL
   "v"					; 11 : OBJC_INTERFACE_DECL
   "s"					; 12 : OBJC_CATEGORY_DECL
   "s"					; 13 : OBJC_PROTOCOL_DECL
   "v"					; 14 : OBJC_PROPERTY_DECL
   "v"					; 15 : OBJC_IVAR_DECL
   "f"					; 16 : OBJC_INSTANCE_METHOD_DECL
   "f"					; 17 : OBJC_CLASS_METHOD_DECL
   "s"					; 18 : OBJC_IMPLEMENTATION_DECL
   "s"					; 19 : OBJC_CATEGORY_IMPL_DECL
   "t"					; 20 : TYPEDEF_DECL
   "f"					; 21 : CXX_METHOD
   "N"					; 22 : NAMESPACE
   "s"					; 23 : LINKAGE_SPEC
   "f"					; 24 : CONSTRUCTOR
   "f"					; 25 : DESTRUCTOR
   "f"					; 26 : CONVERSION_FUNCTION
   "v"					; 27 : TEMPLATE_TYPE_PARAMETER
   "v"					; 28 : TEMPLATE_NON_TYPE_PARAMETER
   "v"					; 29 : TEMPLATE_TEMPLATE_PARAMETER
   "f"					; 30 : FUNCTION_TEMPLATE
   "C"					; 31 : CLASS_TEMPLATE
   "C"					; 32 : CLASS_TEMPLATE_PARTIAL_SPECIALIZATION
   "a"					; 33 : NAMESPACE_ALIAS
   "s"					; 34 : USING_DIRECTIVE
   "s"					; 35 : USING_DECLARATION
   "a"					; 36 : TYPE_ALIAS_DECL
   "s"					; 37 : OBJC_SYNTHESIZE_DECL
   "s"					; 38 : OBJC_DYNAMIC_DECL
   "s"					; 39 : CXX_ACCESS_SPEC_DECL
   ])

(defun flower-kind-id-to-name (kind-id)
  "Return KIND-ID's name."
  (condition-case err
      (elt flower-kind-id-name-map kind-id)
    (error "NOT_IMPLEMENTED")))

(defun flower-kind-id-to-desctiption (kind-id)
  "Return disctiption about KIND-ID."
  (condition-case err
      (elt flower-kind-id-name-map kind-id)
    (error "Not specified")))

(defun flower-kind-id-to-ac-symbol (kind-id)
  "Return AC symbol for candidates list from KIND-ID."
  (condition-case err
      (elt flower-kind-id-ac-symbol-map kind-id)
    (error "-")))

(provide 'flower-definitions)

;;; flower-definitions.el ends here
