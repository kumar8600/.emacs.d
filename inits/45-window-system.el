;;; 45-window-system.el --- My window system configuration

;; Author: kumar8600 <kumar8600@gmail.com>

;; Copyright (c) 2014 by kumar8600
;; All rights reserved.

;;; Commentary:

;;; Code:

(require 'cl-lib)

;; 初期フレームの設定
(setq default-frame-alist
      (append
       '((left                . 500)
	 (top                 . 100)
	 (width               . 100)	; フレーム幅(文字数)
	 (height              . 50))	; フレーム高(文字数)
       default-frame-alist))

(cl-case system-type
  ;; Mac用
  (darwin
   ;; 英語
   (set-face-attribute 'default nil
		       :family "Monaco" ;; font
		       :height 105)    ;; font size

   ;; 日本語
   ;; 日本語をヒラギノ丸ゴProNにする
   (set-fontset-font "fontset-default"
		     'japanese-jisx0208
		     '("Hiragino Maru Gothic ProN"))
   ;; 半角カナをヒラギノ丸ゴProNにする
   (set-fontset-font "fontset-default"
		     'katakana-jisx0201
		     '("Hiragino Maru Gothic ProN"))

   ;; 半角と全角の比を1:2に
					;(setq face-font-rescale-alist '(("Hiragino.*" . 1.2)))
   )
  ;; Win用
  (windows-nt
   ;; 英語
   (set-face-attribute 'default nil
		       :family "Consolas" ;; font
		       :height 100))    ;; font size
  ;; GNU Linux用
  (gnu/linux
   (set-face-attribute 'default nil
		       :height 100)))


;;; 45-window-system.el ends here
