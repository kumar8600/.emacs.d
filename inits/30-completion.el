;;; 40-completion.el --- My emacs configuration for completion.

;; Author: kumar8600 <kumar8600@gmail.com>

;; Copyright (c) 2014 by kumar8600
;; All rights reserved.

;;; Commentary:

;;; Code:

(require 'cmake-mode)
(require 'company)

(add-hook 'cmake-mode-hook #'company-mode)

(require 'auto-complete-config)
(ac-config-default)

;;; 40-completion.el ends here
