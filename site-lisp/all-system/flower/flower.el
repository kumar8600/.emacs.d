;;; flower.el --- Minor mode powered by libclang.

;; Author: kumar8600 <kumar8600@gmail.com>
;; URL: https://github.com/kumar8600/flower
;; Version: 0.0.1
;; Package-Requires: ((epc "0.1.1") (auto-complete "1.4.0") (popup "0.5.0") (yasnippet "0.8.0") (flycheck "0.17")

;; Copyright (c) 2014 by kumar8600
;; All rights reserved.

;; Redistribution and use in source and binary forms, with or without modification,
;; are permitted provided that the following conditions are met:

;; * Redistributions of source code must retain the above copyright notice, this
;;   list of conditions and the following disclaimer.

;; * Redistributions in binary form must reproduce the above copyright notice, this
;;   list of conditions and the following disclaimer in the documentation and/or
;;   other materials provided with the distribution.

;; * Neither the name of the kumar8600 nor the names of its
;;   contributors may be used to endorse or promote products derived from
;;   this software without specific prior written permission.

;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
;; ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
;; DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
;; ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
;; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
;; ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
;; (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
;; SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

;;; Commentary:

;;; Code:

(require 'epc)
(require 'flower-definitions)
(require 'flower-parse)
(require 'flower-timer)
(require 'flower-typeof)
(require 'flower-diagnostics)
(require 'flower-fixit)
(require 'flower-includes)
(require 'flower-completion)

;;;;

(defconst flower-hooks-alist
  '((after-change-functions           . flower-handle-change)
    (window-configuration-change-hook . flower-handle-window-configuration-change))
  "Hooks needs to hook.")

(defvar flower-load-file-name (or load-file-name
				  (buffer-file-name)))
(defvar flower-server-path (expand-file-name "flower-server.py"
					     (file-name-directory
					      flower-load-file-name)))

(defun flower-start ()
  "Start flower."
  (unless (epc:live-p flower-epc)
    (setq flower-epc
	  (epc:start-epc "python"
			 (list flower-server-path))))
  ;; config server
  (flower-server-initialize)
  ;; add hooks
  (mapc (lambda (item)
	  (add-hook (car item) (cdr item) nil t))
	flower-hooks-alist)
  ;; idle handling
  (flower-set-idle-timer)
  (flower-parse-current-buffer)
  (flower-enable-completion))

(defun flower-stop ()
  "Stop flower."
  (flower-disable-completion)
  ;; remove hooks
  (mapc (lambda (item)
	  (remove-hook (car item) (cdr item) t))
	flower-hooks-alist)
  ;; cancel idle handling
  (flower-clear-idle-timer)
  (when (epc:live-p flower-epc)
    (epc:stop-epc flower-epc)))

(defvar flower-mode-line " Fl"
  "Lighter of flower mode.")

(define-minor-mode flower-mode
  "Minor mode for libclang integration."
  :init-value nil
  :lighter flower-mode-line
  :group 'flower
  :require 'flower
  (if flower-mode
      (flower-start)
    (flower-stop)))

;;;; Buffer changing handler
(defun flower-handle-change (beg end len)
  "Handle a changing input between BEG and END.

But BEG and END and LEN are ignored."
  (when flower-mode
    (setq flower-need-to-reparse-flag t)))

(defun flower-handle-window-configuration-change ()
  "Handle a window configuration changing."
  nil)
  ;; (when flower-mode
  ;;   (flower-parse-current-buffer)))

;;;; Server configuration
(defun flower-server-initialize ()
  "Set additional library search path to find libclang."
  (flower-pre-check)
  (epc:call-sync flower-epc
		 'initialize
		 (list flower-libclang-directory-path)))
  ;; (deferred:$
  ;;   (epc:call-deferred flower-epc
  ;; 		       'initialize
  ;; 		       (list flower-libclang-directory-path))))


(provide 'flower)

;;; flower.el ends here
