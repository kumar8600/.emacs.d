;;; 30-key-act.el --- My emacs configurations for key actions.

;; Author: kumar8600 <kumar8600@gmail.com>

;; Copyright (c) 2014 by kumar8600
;; All rights reserved.

;;; Commentary:

;;; Code:

;;
;;キーの挙動
;;

;; launch with window focus
(if window-system
    (x-focus-frame nil))

;; 改行時にインデント
(global-set-key "\C-m" 'newline-and-indent)
(global-set-key "\C-j" 'newline)

(add-hook 'c-mode-common-hook
          '(lambda ()
             (local-set-key "\C-m" 'reindent-then-newline-and-indent)))

(add-hook 'sh-mode-hook
          '(lambda ()
             (local-set-key "\C-m" 'reindent-then-newline-and-indent)))

;; autopair
(require 'autopair)
(add-hook 'c-mode-common-hook
	  #'(lambda () (autopair-mode)))

;; M-kanji で、何もしないよう定義
(global-set-key [M-kanji] 'ignore)

;;; 30-key-act.el ends here
