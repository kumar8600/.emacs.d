;;; flower-parse.el --- Flower mode back-end for source code parsing.

;; Author: kumar8600 <kumar8600@gmail.com>

;; Copyright (c) 2014 by kumar8600
;; All rights reserved.

;; Redistribution and use in source and binary forms, with or without modification,
;; are permitted provided that the following conditions are met:

;; * Redistributions of source code must retain the above copyright notice, this
;;   list of conditions and the following disclaimer.

;; * Redistributions in binary form must reproduce the above copyright notice, this
;;   list of conditions and the following disclaimer in the documentation and/or
;;   other materials provided with the distribution.

;; * Neither the name of the kumar8600 nor the names of its
;;   contributors may be used to endorse or promote products derived from
;;   this software without specific prior written permission.

;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
;; ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
;; DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
;; ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
;; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
;; ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
;; (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
;; SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

;;; Commentary:

;;; Code:

(require 'epc)
(require 'flower-definitions)

(defun flower-parse-current-buffer ()
  "Parse with libclang current buffer."
  (interactive)
  (flower-pre-check)
  (setq flower-need-to-reparse-flag nil)
  (deferred:$
    (epc:call-deferred flower-epc 'parse_unsaved_file (list (buffer-file-name)
							    (buffer-substring-no-properties
							     (point-min) (point-max))))))

(defun flower-reparse-current-buffer ()
  "Reparse with libclang current buffer."
  (interactive)
  (flower-pre-check)
  (setq flower-need-to-reparse-flag nil)
  (deferred:$
    (epc:call-deferred flower-epc 'reparse_unsaved_file (list (buffer-file-name)
							      (buffer-substring-no-properties
							       (point-min) (point-max))))))

(defun flower-with-reparse (func)
  "After reparse current buffer, call FUNC.

FUNC have a argument RESULT."
  (if flower-need-to-reparse-flag
      (progn
	(flower-pre-check)
	(setq flower-need-to-reparse-flag nil)
	(deferred:$
	  (epc:call-deferred flower-epc 'reparse_unsaved_file (list (buffer-file-name)
								    (buffer-substring-no-properties
								     (point-min) (point-max))))
	  (deferred:nextc it
	    func)))
    (funcall func nil)))


(provide 'flower-parse)

;;; flower-parse.el ends here
