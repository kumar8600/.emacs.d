;;; 30-checker.el --- My emacs configuration for syntax/style checker.

;; Author: kumar8600 <kumar8600@gmail.com>

;; Copyright (c) 2014 by kumar8600
;; All rights reserved.

;;; Commentary:

;;; Code:

(require 'flycheck)
(add-hook 'after-init-hook #'global-flycheck-mode)

;; PHP CodeSniffer を無効化
(custom-set-variables
 '(flycheck-php-phpcs-executable "true"))

;;; 30-checker.el ends here
