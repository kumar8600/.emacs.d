;;; 01-basic-configurations.el --- My basic configurations and definitions for emacs.

;; Author: kumar8600 <kumar8600@gmail.com>

;; Copyright (c) 2014 by kumar8600
;; All rights reserved.

;;; Commentary:

;;; Code:

;; デフォルト文字コードの指定
(set-default-coding-systems 'utf-8-dos)

;; ユーザーの定義
;;; フルネーム
(setq user-full-name "kumar8600")
;;; メールアドレス
(setq user-mail-address "kumar8600@gmail.com")

;; バックアップディレクトリを指定
(setq backup-directory-alist
      `((".*" . ,temporary-file-directory)))
(setq auto-save-file-name-transforms
      `((".*" ,temporary-file-directory t)))

;;; 01-basic-configurations.el ends here
