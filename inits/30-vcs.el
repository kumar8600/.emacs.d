;;; 30-vcs.el --- My emacs configuration for version control system (like Git).

;; Author: kumar8600 <kumar8600@gmail.com>

;; Copyright (c) 2014 by kumar8600
;; All rights reserved.

;;; Commentary:

;;; Code:

(require 'magit)

;; When using in Windows, password prompt seems don't works well.
;; > https://github.com/magit/magit/wiki/FAQ#windows
(when (eq system-type
	  'windows-nt)
  (setenv "GIT_ASKPASS" "git-gui--askpass"))

;;; 30-vcs.el ends here
