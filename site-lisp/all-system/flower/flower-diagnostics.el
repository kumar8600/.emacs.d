;;; flower-diagnostics.el --- Flower mode back-end for diagnostics.

;; Author: kumar8600 <kumar8600@gmail.com>

;; Copyright (c) 2014 by kumar8600
;; All rights reserved.

;; Redistribution and use in source and binary forms, with or without modification,
;; are permitted provided that the following conditions are met:

;; * Redistributions of source code must retain the above copyright notice, this
;;   list of conditions and the following disclaimer.

;; * Redistributions in binary form must reproduce the above copyright notice, this
;;   list of conditions and the following disclaimer in the documentation and/or
;;   other materials provided with the distribution.

;; * Neither the name of the kumar8600 nor the names of its
;;   contributors may be used to endorse or promote products derived from
;;   this software without specific prior written permission.

;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
;; ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
;; DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
;; ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
;; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
;; ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
;; (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
;; SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

;;; Commentary:

;;; Code:

(require 'epc)
(require 'flycheck)
(require 'flower-definitions)
(require 'flower-parse)

(defun flower-diagnostics ()
  "Show diagnostics."
  (interactive)
  (flower-with-reparse
   (lambda (result)
     (deferred:$
       (epc:call-deferred flower-epc 'get_diagnostics nil)
       (deferred:nextc it
	 (lambda (result)
	   (print result)))))))

(defun flower-diagnostics-result-to-string (result)
  "Return string formed like clang's output from RESULT."
  (if result
      (mapconcat (lambda (item)
		   (let ((diag (car item)))
		     (concat (format "%s:%d:%d: %s: %s"
				     (car diag)
				     (cadr diag)
				     (caddr diag)
				     (cadddr diag)
				     (nth 4 diag))
			     (when (car-safe (cdr item))
			       " [Hint: Fixit is available.]"))))
		 result
		 "\n")
    ""))

(defun flower-diagnostics-flycheck-handle-result (buffer result)
  "Handle flower diagnostic for BUFFER's RESULT."
  (let ((files (list (buffer-file-name)))
	(exit-status (if result
			 1
		       0))
	(output (flower-diagnostics-result-to-string result)))
  (when (buffer-live-p buffer)
    (with-current-buffer buffer
      (flycheck-report-status "")
      (when flycheck-mode
	(condition-case err
	    (flycheck-finish-syntax-check 'c/c++-flower exit-status files output)
	  (error
	   (flycheck-report-error)
	   (signal (car err) (cdr err)))))))))

(defun flower-diagnostics-flycheck-start-checker ()
  "Start flycheck checker for Flower mode."
  (lexical-let ((buffer (current-buffer)))
    (flower-with-reparse
     (lambda (result)
       (deferred:$
	 (epc:call-deferred flower-epc 'get_diagnostics nil)
	 (deferred:nextc it
	   (lambda (result)
	     (setq flower-diagnostics-current result)
	     (flower-diagnostics-flycheck-handle-result buffer result))))))))

(defadvice flycheck-start-checker (around flower-flycheck-start-checker-advice activate)
  "Flower using `epc' for transaction but flycheck using pipe so specializing is needed."
  (if (and (eq 'c/c++-flower
	       (ad-get-arg 0))
	   (eval 'flower-mode))
      (flower-diagnostics-flycheck-start-checker)
    ad-do-it))

(flycheck-define-checker c/c++-flower
  "A C/C++ syntax checker part of Flower mode.

See URL `https://github.com/kumar8600/flower-mode'."
  :command ("echo"
	    "unko")
  :error-patterns
  ((info line-start (file-name) ":" line ":" column
	 ": note: " (message) line-end)
   (warning line-start (file-name) ":" line ":" column
	    ": warning: " (message) line-end)
   (error line-start (file-name) ":" line ":" column
	  ": " (or "fatal error" "error") ": " (message) line-end))
  :modes (c-mode c++-mode))

(add-to-list 'flycheck-checkers 'c/c++-flower)

(provide 'flower-diagnostics)

;;; flower-diagnostics.el ends here
