;;; 60-twitter.el --- My emacs configuration for twitter

;; Author: kumar8600 <kumar8600@gmail.com>

;; Copyright (c) 2014 by kumar8600
;; All rights reserved.

;;; Commentary:

;; Requires tw.
;; $ gem install tw

;;; Code:

(defun my-tw-tweet (msg)
  "Tweet MSG via tw."
  (interactive "sTweet via tw: ")
  (start-process-shell-command "emacs-my-tw-tweet"
			       "*my-tw-tweet*"
			       (format "%s %s --yes"
				       (executable-find "tw")
				       msg)))

(defun my-tw-stream ()
  "Show userstream via tw."
  (interactive)
  (async-shell-command (format "%s --stream"
			       (executable-find "tw"))))

(global-set-key "\C-ct" 'my-tw-tweet)
(global-set-key "\C-cs" 'my-tw-stream)

;;; 60-twitter.el ends here
