;;; 10-package.el --- My emacs configuration for package system.

;; Author: kumar8600 <kumar8600@gmail.com>

;; Copyright (c) 2014 by kumar8600
;; All rights reserved.

;;; Commentary:

;;; Code:

(require 'cl-lib)

;;
;; package
;;
(require 'package)
(add-to-list 'package-archives '("melpa" . "http://melpa.milkbox.net/packages/"))
(add-to-list 'package-archives '("marmalade" . "http://marmalade-repo.org/packages/"))

(package-initialize)

;;
;; パッケージの自動インストール
;;
(defvar installing-package-list
  '(
    ;; ここに使っているパッケージを書く。
    ;;el-getで管理するものは20-el-get.elに書いてあるので注意
    esup
    epl
    helm
    ac-helm
    yasnippet
    company
    php-mode
    mustache-mode
    scala-mode
    rust-mode
    toml-mode
    markdown-mode
    scss-mode
    haskell-mode
    cmake-mode
    d-mode
    yaml-mode
    slime
    slime-company
    google-c-style
    open-junk-file
    autopair
    solarized-theme
    smart-mode-line
    sr-speedbar
    tabbar
    quickrun
    flycheck
    flycheck-color-mode-line
    undo-tree
    linum-off
    hlinum
    magit
    monokai-theme
    ))

(defun my-install-packages ()
  "Install packages defined by `installing-package-list`."
  (interactive)
  (let ((not-installed (cl-loop for x in installing-package-list
				when (not (package-installed-p x))
				collect x)))
    (when not-installed
      (package-refresh-contents)
      (dolist (pkg not-installed)
        (package-install pkg)))))

(my-install-packages)

;;; 10-package.el ends here
