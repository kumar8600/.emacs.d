;;; 30-buffer.el --- My emacs configurations for buffer.

;; Author: kumar8600 <kumar8600@gmail.com>

;; Copyright (c) 2014 by kumar8600
;; All rights reserved.

;;; Commentary:

;;; Code:

;;
;;バッファ設定
;;

;; バッファ自動再読み込み
(global-auto-revert-mode 1)

;; 同名ファイルのバッファ名の識別文字列を変更
(require 'uniquify)
(setq uniquify-buffer-name-style 'post-forward-angle-brackets)

;; 行番号表示
;; linum-mode
(global-linum-mode 1)
(setq linum-format "%4d: ")
;; 場合によっては行番号を表示しない
(require 'linum-off)
;; 現在行の行番号を強調
;; (hlinum-activate)
(custom-set-faces
 '(linum-highlight-face((t (:foreground "orange3")))))

;; undo-treeモードの設定
(when (require 'undo-tree nil t)
    (global-undo-tree-mode))

;;
;; コードスタイル
;;

;; google-c-style
(require 'google-c-style)
(add-hook 'c-mode-common-hook 'google-set-c-style)

;; D言語のタブ
(add-hook 'd-mode-hook
          (lambda ()
            (c-set-style "bsd")
            (setq c-basic-offset 4)
            (c-set-offset 'statement-cont 'c-lineup-math)
            )
	  )

;;
;; ドキュメンテーションコメント
;;

;; doxymacs
(require 'doxymacs nil t)

;;; 30-buffer.el ends here
