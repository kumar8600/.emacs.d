;;; 50-user-interface.el --- My emacs configuration for user interface.

;; Author: kumar8600 <kumar8600@gmail.com>

;; Copyright (c) 2014 by kumar8600
;; All rights reserved.

;;; Commentary:

;;; Code:

(require 'cl-lib)

;; ツールバーを無効化
(tool-bar-mode 0)
;; メニューバーを無効化（nwのみ）
(cl-flet ((disable-menu-bar ()
			    (if window-system
				(menu-bar-mode 1)
			      (menu-bar-mode -1))))
  ;;; 通常起動時
  (disable-menu-bar)
  ;;; emacsclientをGUIで起動したら強制的に有効になってしまうので接続時にフックした
  (add-hook 'server-visit-hook #'disable-menu-bar))

;;
;; タブ
;;

(require 'tabbar)

;; キーバインド
(global-set-key [C-tab] 'tabbar-forward-tab)  ; 次のタブ
(global-set-key [C-S-tab] 'tabbar-backward-tab) ; 前のタブ
; for nw
(global-set-key (kbd "C-c <right>") 'tabbar-forward-tab) ; 次のタブ
(global-set-key (kbd "C-c <left>") 'tabbar-backward-tab) ; 前のタブ

;; タブ上でマウスホイールを使わない
(tabbar-mwheel-mode nil)

;; 左側のボタンを消す
(dolist (btn '(tabbar-buffer-home-button
               tabbar-scroll-left-button
               tabbar-scroll-right-button))
  (set btn (cons (cons "" nil)
                 (cons "" nil))))

;; タブ同士の間隔
(setq tabbar-separator '(1.5))

;; 色設定
(set-face-attribute ; バー自体の色
  'tabbar-default nil
  :background (face-attribute 'mode-line :background)
  :family "Inconsolata"
  :height 1.0)
(set-face-attribute ; アクティブなタブ
  'tabbar-selected nil
  :background (face-attribute 'tabbar-default :background)
  :foreground "black"
  :weight 'bold
  :box nil)
(set-face-attribute ; 非アクティブなタブ
 'tabbar-unselected nil
 :background (face-attribute 'tabbar-default :background)
 :foreground "gray60"
 :box nil)

;; * で始まるバッファとdiredを"emacs"グループ、その他を"user"グループに分類
(defun my-tabbar-buffer-groups () ;; customize to show all normal files in one group
   "Return the name of the tab group names the current buffer belongs to.

 There are two groups: Emacs buffers (those whose name starts with '*', plus
 dired buffers), and the rest.  This works at least with Emacs v24.2 using
 tabbar.el v1.7."
   (list (cond ((string-equal "*" (substring (buffer-name) 0 1)) "emacs")
               ((eq major-mode 'dired-mode) "emacs")
               (t "user"))))
 (setq tabbar-buffer-groups-function 'my-tabbar-buffer-groups)

;; 有効
(tabbar-mode)


;;; 50-user-interface.el ends here
