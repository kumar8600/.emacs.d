;;
;; quickrun
;;

(require 'quickrun)

;; C++11 に対応
(quickrun-add-command "c++/c11"
		      '((:command . "clang++")
			(:cmdopt  . "-Wall -Wextra -pedantic")
			(:exec    . ("%c -stdlib=libc++ -std=c++11 %o -o %e %s"
				     "%e %a"))
			(:remove  . ("%e")))
		      :default "c++")

(global-set-key (kbd "<f5>") 'quickrun)
